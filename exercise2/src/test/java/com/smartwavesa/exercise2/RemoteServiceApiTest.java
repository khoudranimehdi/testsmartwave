package com.smartwavesa.exercise2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

// public visibility needed
@ExtendWith(MockitoExtension.class)
public class RemoteServiceApiTest {

    @InjectMocks
    private RemoteApiService remoteApiService;

    @BeforeEach
    public void setup() {
        remoteApiService = new RemoteApiService(new RestTemplate());
    }
    @Test
    public void testServiceReturnOk() {
        Integer expectedCount = remoteApiService.callRemoteService();
        assertEquals(10, expectedCount);
    }

    @Test
    public void testServiceReturnOtherThanOk() {
        Integer expectedCount = remoteApiService.callRemoteService();
        assertEquals(0, expectedCount);
    }
}
