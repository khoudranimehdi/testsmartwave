package com.smartwavesa.exercise2;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RemoteApiService {

    private final RestTemplate restTemplate;

    public RemoteApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Integer callRemoteService() {
        int count;
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("https://test.smartwavesa.com", String.class);
        if(responseEntity.getStatusCode() == HttpStatus.OK) {
            count = 10;
        } else {
            count = 0;
        }
        return count;
    }
}
