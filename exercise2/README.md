# Exercise 2

Write a JUnit 5 unit test for the `RemoteApiService` by using the prepared test `RemoteApiServiceTest`

All tests should pass by running `mvn test`

_Disclaimer: the exercise code does not follow best practices on purpose_ 
