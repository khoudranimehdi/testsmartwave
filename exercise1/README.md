# Exercise 1

Fix the code to run successfully the command. The class `Exercise1Application` must not be changed
```
mvn compile spring-boot:run -Dspring-boot.run.profiles=loc
```
_Disclaimer: the exercise code does not follow best practices on purpose_
