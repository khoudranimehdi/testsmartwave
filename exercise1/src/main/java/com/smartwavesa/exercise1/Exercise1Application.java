package com.smartwavesa.exercise1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class Exercise1Application implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(Exercise1Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Exercise1Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("Exercise 1 is a success");
    }

    @Profile("loc")
    @Configuration
    @EnableConfigurationProperties(MyProperties.class)
    static class AppConfig {
    }

}
