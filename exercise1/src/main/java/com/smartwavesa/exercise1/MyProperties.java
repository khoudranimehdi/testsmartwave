package com.smartwavesa.exercise1;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ConstructorBinding
@ConfigurationProperties(prefix = "smw")
@Validated
public class MyProperties {
    @NotNull
    private final Nested nested;

    public MyProperties(@NotNull Nested nested) {
        this.nested = nested;
    }

    public Nested getNested() {
        return nested;
    }

    static class Nested {
        @NotBlank
        private final String value;

        public Nested(@NotBlank String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
