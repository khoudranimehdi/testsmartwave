package com.smartwavesa.exercise3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Exercise3Application {

    public static void main(String[] args) {
        SpringApplication.run(Exercise3Application.class, args);
    }

    @Configuration
    static class MyConfig {
        @Bean
        public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
            return restTemplateBuilder
                    .rootUri("https://api.smartwavesa.com")
                    .build();
        }
    }

    @Configuration
    static class MyOtherConfig {
        @Bean
        public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
            return restTemplateBuilder
                    .rootUri("https://microservice.smartwavesa.com")
                    .build();
        }
    }

    @Service
    static class MyService {
        private final RestTemplate apiRestTemplate;

        private final RestTemplate microserviceRestTemplate;

        MyService(RestTemplate apiRestTemplate, RestTemplate microserviceRestTemplate) {
            this.apiRestTemplate = apiRestTemplate;
            this.microserviceRestTemplate = microserviceRestTemplate;
        }

        public RestTemplate getApiRestTemplate() {
            return apiRestTemplate;
        }

        public RestTemplate getMicroserviceRestTemplate() {
            return microserviceRestTemplate;
        }
    }
}
