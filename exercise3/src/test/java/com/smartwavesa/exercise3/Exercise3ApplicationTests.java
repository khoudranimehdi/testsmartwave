package com.smartwavesa.exercise3;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class Exercise3ApplicationTests {

    @Autowired
    private Exercise3Application.MyService myService;

    @Test
    void contextLoads() {
        assertNotNull(myService.getApiRestTemplate());
        assertNotNull(myService.getMicroserviceRestTemplate());
    }

}
