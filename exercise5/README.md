# Exercise 5

Implement a CRUD REST API on the `Company` entity. The API contains these operations:
- Create a company
- Get a company by id
- Update a company
- Delete a company

Note: No bulk operation\
Note2: No test

The persistence is done with an in-memory database H2. The database schema is already created.

_Disclaimer: the exercise code does not follow best practices on purpose_
 
