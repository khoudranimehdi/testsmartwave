package service;

import com.smartwavesa.exercise5.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.CompanyRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public abstract class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public Company getCompanyById(Long id){
        Optional<Company> company = companyRepository.findById(id);
        return company.orElse(null);
    }

    public boolean deleteCompanyById(Long id){
        boolean isDeleted = false;
        Optional<Company> company = companyRepository.findById(id);
        if (company.isPresent()){
            isDeleted = true;
        }
        return isDeleted;
    }

    public void saveOrUpdate(Company company)
    {
        companyRepository.save(company);
    }
}
