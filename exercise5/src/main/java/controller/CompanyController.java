package controller;

import com.smartwavesa.exercise5.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.CompanyService;

import java.util.Optional;

public class CompanyController {

    @Autowired
    CompanyService companyService;

    @PostMapping("/company")
    private Long saveCompany(@RequestBody Company company)
    {
        companyService.saveOrUpdate(company);
        return company.getId();
    }

    @GetMapping("/company/{companyid}")
    private Company getCompanyById(@PathVariable("companyId") Long companyId)
    {
        return companyService.getCompanyById(companyId);
    }

    @DeleteMapping("/company/{companyid}")
    private void deleteBook(@PathVariable("companyid") Long companyid)
    {
        companyService.deleteCompanyById(companyid);
    }

    @PutMapping("/company")
    private Company updateCompany(@RequestBody Company company)
    {
        companyService.saveOrUpdate(company);
        return company;
    }
}
