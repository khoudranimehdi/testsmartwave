# Smartwave Technical Test

The requirements are:
- Java
- Maven
- Git

The test consists in several exercises. Each exercise has its own folder and instructions are in README.md:
- Exercise 1: Spring Boot
- Exercise 2: Unit Test
- Exercise 3: Spring Boot
- Exercise 4: JPA
- Exercise 5: REST, Spring Boot

**To provide your answer, just push your commits into the candidate branch.**
