package com.smartwavesa.exercise4;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="COMPANY")
@Data
public class Company {

    private Long id;

    private String name;

    @OneToMany(mappedBy="company")
    private List<Employee> employeeSet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployeeSet() {
        return employeeSet;
    }

    public void setEmployeeSet(List<Employee> employeeSet) {
        this.employeeSet = employeeSet;
    }

    public Company(String name, List<Employee> employeeSet) {
        this.name = name;
        this.employeeSet = employeeSet;
    }
    public Company() {
    }
}
