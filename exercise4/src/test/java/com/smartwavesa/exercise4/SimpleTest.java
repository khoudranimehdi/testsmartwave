package com.smartwavesa.exercise4;

import net.bytebuddy.matcher.ElementMatcher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class SimpleTest {

    @Test
    void testCreateRelationship() {
        // create the relationship using the Company and Employee classes
        Company company = new Company();
        Employee employee = new Employee();
        List<Employee> employees = new ArrayList<Employee>();

        employee.setName("Candidate");
        company.setName("Smartwave");

        employee.setCompany(company);
        employees.add(employee);
        company.setEmployeeSet(employees);


        // assert that the name of the company is Smartwave
        assertEquals("Smartwave", company.getName());
        // assuming the company has one employee
        // assert that the employee name is Candidate
        assertEquals("Candidate", employee.getName());
        // assert that the employee's company is Smartwave
        assertEquals("Smartwave", employee.getCompany().getName());
    }
}
