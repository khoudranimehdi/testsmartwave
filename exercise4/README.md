# Exercise 4

Add the correct JPA annotation and necessary attributes in the two classes `Company` and `Employee` to model a JPA bidirectional relationship:
- one company has several employees
- one employee belongs to only one company

Make the test pass with `mvn test` 

Note: The table naming is not important.

_Disclaimer: the exercise code does not follow best practices on purpose_
